package com.koch.rahman.nflcapcalculator;

import android.annotation.SuppressLint;
import android.os.StrictMode;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionClass {
    private String ip = "136.33.110.214";
    private String driverClass = "net.sourceforge.jtds.jdbc.driver";
    private String db = "CS461_App_DB";
    private String un = "bmrghb";
    private String pwd = "N.F.L.cat";

    @SuppressLint("New Api")
    public Connection CONN(){
        // Returns the Connection object for connecting with the serve
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Connection conn = null;
        String connUrl = null;

        try {
            Class.forName(driverClass);
            connUrl = "jdbc:jtds:sqlserver://" + ip + ";databaseName=" + db + ";user=" + un + ";password=" + pwd + ";";
            conn = DriverManager.getConnection(connUrl);
        }
        catch (SQLException se){
            System.out.println("ERROR: SQL Exception Triggered in Connection Class.\n");
        }
        catch (Exception ex){
            System.out.println("ERROR: Unknown Exception Triggered in Connection Class.\n");
        }

        return conn;
    }

}

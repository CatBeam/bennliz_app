/*
PURPOSE:
    Programmatically constructs roster rows from a given array of NFLPlayer Objects
 */

package com.koch.rahman.nflcapcalculator;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class RosterAdapter extends ArrayAdapter{

    private final Activity context;
    private final NFLPlayer[] rosterArray;
    public RosterAdapter(Activity context, NFLPlayer[] rosterArrayParams){
        super(context, R.layout.roster_row, rosterArrayParams);
        this.context = context;
        this.rosterArray = rosterArrayParams;
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.roster_row, null, true);

        // Get Row's TextView Refs
        TextView nameTV = (TextView) rowView.findViewById(R.id.name_textView);
        TextView positionTV = (TextView) rowView.findViewById(R.id.position_textView);
        TextView accruedSeasonsTV = (TextView) rowView.findViewById(R.id.accruedSeasons_textView);
        TextView baseSalaryTV = (TextView) rowView.findViewById(R.id.baseSalary_textView);
        TextView bonusTV = (TextView) rowView.findViewById(R.id.bonus_textView);
        TextView capNumberTV = (TextView) rowView.findViewById(R.id.capNumber_textView);

        //Find Player's total bonus earnings
        int bonusSum = (rosterArray[position].getProratedBonus()
                + rosterArray[position].getRosterBonus()
                + rosterArray[position].getWorkoutBonus());

        // Set Player Row's TextView display Values
        nameTV.setText(rosterArray[position].getName());
        positionTV.setText(rosterArray[position].getPosition());
        accruedSeasonsTV.setText(String.valueOf(rosterArray[position].getAccruedSeasons()));
        baseSalaryTV.setText(String.valueOf(rosterArray[position].getSalary()));
        bonusTV.setText(String.valueOf(bonusSum));
        capNumberTV.setText(String.valueOf(rosterArray[position].getCapNumber()));

        // Return the Player Row's Reference
        return rowView;
    }
}

package com.koch.rahman.nflcapcalculator;
/*
PURPOSE:
    This class is meant to act as a Data Transfer Object for building display values
    for an NFL player from SQL Query Result Sets
 */
public class NFLPlayer {
    private String name;
    private String position;
    private int accruedSeasons;
    private int salary;
    private int guaranteedSalary;
    private int proratedBonus;
    private int rosterBonus;
    private int workoutBonus;
    private int capNumber;
    private int cutDeadMoney;
    private String teamName;

    public NFLPlayer(String fn, String pos, int as, int sal, int gsal, int pb, int rb, int wb, int cn, int cdm, String tn){
        this.name = fn;
        this.position = pos;
        this.accruedSeasons = as;
        this.salary = sal;
        this.guaranteedSalary = gsal;
        this.proratedBonus = pb;
        this.rosterBonus = rb;
        this.workoutBonus = wb;
        this.capNumber = cn;
        this.cutDeadMoney = cdm;
        this.teamName = tn;
    }

    public String getName() {
        return name;
    }

    public String getPosition() {
        return position;
    }

    public int getAccruedSeasons() {
        return accruedSeasons;
    }

    public int getSalary() {
        return salary;
    }

    public int getGuaranteedSalary() {
        return guaranteedSalary;
    }

    public int getProratedBonus() {
        return proratedBonus;
    }

    public int getRosterBonus() {
        return rosterBonus;
    }

    public int getWorkoutBonus() {
        return workoutBonus;
    }

    public int getCapNumber() {
        return capNumber;
    }

    public int getCutDeadMoney() {
        return cutDeadMoney;
    }

    public String getTeamName() {
        return teamName;
    }
}
